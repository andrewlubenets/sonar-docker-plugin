package fr.hetic.docker;

import static java.util.Arrays.asList;

import org.sonar.api.Plugin;
import org.sonar.api.config.PropertyDefinition;

import fr.hetic.docker.languages.DockerLanguage;
import fr.hetic.docker.languages.DockerQualityProfile;
import fr.hetic.docker.rules.DockerLintRulesDefinition;

public class SonarDockerPlugin implements Plugin {

	@Override
	public void define(Context context) {

		context.addExtensions(DockerLanguage.class, DockerQualityProfile.class);
		context.addExtension(DockerLintRulesDefinition.class);
		context.addExtensions( //
				asList(PropertyDefinition.builder("sonar.lang.patterns.docker") //
				.multiValues(true) //
				.defaultValue("Dockerfile") //
				.category("Docker") //
				.name("Pattern file DockerLint") //
				.description("Patterns supported by DockerLint.") //
				.build()));
	}

}
