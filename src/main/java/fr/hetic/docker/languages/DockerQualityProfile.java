package fr.hetic.docker.languages;

import org.sonar.api.server.profile.BuiltInQualityProfilesDefinition;

import static fr.hetic.docker.rules.DockerLintRulesDefinition.REPO_KEY;

public class DockerQualityProfile implements BuiltInQualityProfilesDefinition {

	@Override
	public void define(Context context) {
		NewBuiltInQualityProfile profile = context.createBuiltInQualityProfile("DockerLint Rules", DockerLanguage.KEY);
		profile.setDefault(true);
		
		profile.activateRule(REPO_KEY, "DL3000");
        profile.activateRule(REPO_KEY, "DL3001");
        profile.activateRule(REPO_KEY, "DL3002");
        profile.activateRule(REPO_KEY, "DL3003");
        profile.activateRule(REPO_KEY, "DL3004");
        profile.activateRule(REPO_KEY, "DL3005");
        profile.activateRule(REPO_KEY, "DL3006");
        profile.activateRule(REPO_KEY, "DL3007");
        profile.activateRule(REPO_KEY, "DL3008");
        profile.activateRule(REPO_KEY, "DL3009");
        profile.activateRule(REPO_KEY, "DL3010");
        profile.activateRule(REPO_KEY, "DL3011");
        profile.activateRule(REPO_KEY, "DL3012");
        profile.activateRule(REPO_KEY, "DL3013");
        profile.activateRule(REPO_KEY, "DL3014");
        profile.activateRule(REPO_KEY, "DL3015");
        profile.activateRule(REPO_KEY, "DL3016");
        profile.activateRule(REPO_KEY, "DL3017");
        profile.activateRule(REPO_KEY, "DL3018");
        profile.activateRule(REPO_KEY, "DL3019");
        profile.activateRule(REPO_KEY, "DL3020");
        profile.activateRule(REPO_KEY, "DL3021");
        profile.activateRule(REPO_KEY, "DL3022");
        profile.activateRule(REPO_KEY, "DL3023");
        profile.activateRule(REPO_KEY, "DL3024");
        profile.activateRule(REPO_KEY, "DL3025");
        profile.activateRule(REPO_KEY, "DL3026");
        profile.activateRule(REPO_KEY, "DL3027");
        profile.activateRule(REPO_KEY, "DL3028");
        profile.activateRule(REPO_KEY, "DL4000");
        profile.activateRule(REPO_KEY, "DL4001");
        profile.activateRule(REPO_KEY, "DL4003");
        profile.activateRule(REPO_KEY, "DL4004");
        profile.activateRule(REPO_KEY, "DL4005");
        profile.activateRule(REPO_KEY, "DL4006");
        profile.activateRule(REPO_KEY, "SC2046");
        profile.activateRule(REPO_KEY, "SC2086");
		
		profile.done();
	}

}
